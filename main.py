from tkinter import Tk , Canvas , Button , Label 
from tkinter.messagebox import showerror
from os import system , getlogin 
from os.path import isfile

user=getlogin()

black="#171a1f"
white="#f5f5f5"
red="#fc7b81"
green="#94f7c5"
yellow="#ffeba6"
blue="#8cc1ff"

main=Tk()
main.title('arnix-byemenu')
main.geometry('800x300')
main.configure(background=black)

main.resizable(False , False)

canvas=Canvas(main , width=800 , height=300 , bg=black)
canvas.pack()

def lock():
    global lockcmd
    slimpath="/etc/pam.d/slimlock"
    betterlockscreen="/usr/bin/betterlockscreen"
    i3lockpath="/usr/bin/i3lock"
    slockpath="/usr/bin/slock"
    
    slockexists=isfile(slockpath)
    i3lockexists=isfile(i3lockpath)
    betterlockscreenexists=isfile(betterlockscreen)
    slimexists=isfile(slimpath)

    if slockexists==True:
        lockcmd="slock"
    elif i3lockexists==True:
        lockcmd="i3lock"
    elif slimexists==True:
        lockcmd="slimlock"
    elif betterlockscreenexists==True:
        lockcmd="betterlockscreen -l"
    else:
        showerror('Error' , 'Please open an issue on https://gitlab.com/arnix-os/byemenu to fix this...')
        main.destroy()

lock()

powerbutton=Button(main , text='''⏻
Shut Down''' , font=("Fira Code Nerd Font Mono" , 9) , background=black , foreground=white , borderwidth=0 , relief="flat" , activeforeground=red , activebackground=black , command=lambda:system("poweroff"))
rebbutton=Button(main , text='''
Restart''' , font=("Fira Code Nerd Font Mono" , 9) , background=black , foreground=white , borderwidth=0 , relief="flat" , activeforeground=green , activebackground=black , command=lambda:system("restart"))
lockbutton=Button(main , text='''
Lock''' , font=("Fira Code Nerd Font Mono" , 9) , background=black , foreground=white , borderwidth=0 , relief="flat" , activeforeground=yellow , activebackground=black , command=lambda:system(f"{lockcmd}"))
logbutton=Button(main , text='''ﴚ
Log Out''' , font=("Fira Code Nerd Font Mono" , 9) , background=black , foreground=white , borderwidth=0 , relief="flat" , activeforeground=blue , activebackground=black , command=lambda:system(f"pkill -U {user}"))

cancbutton=Button(main , text='''ﰸ
Cancel''' , font=("Fira Code Nerd Font Mono" , 9) , background=black , foreground=white , borderwidth=0 , relief="flat" , activeforeground=black , activebackground=red,command=lambda:main.destroy())

canvas.create_window(400 , 50, window=Label(main , text=f"bye {user}" , font=("Fira Code Nerd Font Mono" , 9) , bg=black , fg=white))

canvas.create_window(170 , 150 , window=powerbutton)
canvas.create_window(350 , 150 , window=rebbutton)
canvas.create_window(500 , 150 , window=lockbutton)
canvas.create_window(650 , 150 , window=logbutton)

canvas.create_window(400 , 250, window=cancbutton)

main.mainloop()
