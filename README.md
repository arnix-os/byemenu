# byemenu

A simple , yet minimal powermenu written in python

# Features:-

- Screen Locking
- Shutting down system
- Restarting system

# Expected features:-

- [ ] Themes
- [ ] More lock screens

# Preview:-

![main](opt/preview.png)
